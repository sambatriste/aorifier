import React from 'react';
import { Route } from 'react-router-dom';
import Home from './Home';
import Register from './Register';
import ImageEdit from './ImageEdit';
import TagList from './TagList';
import { Images, Tags } from './Model';
import RealtimeDatabaseWrapper from '../fb/RealtimeDatabaseWrapper';

type AppContentProps = {
  images: Images;
  tags: Tags;
  db: RealtimeDatabaseWrapper;
};

const AppContent: React.FC<AppContentProps> = ({
  images,
  tags,
  db
}: AppContentProps) => (
  <div style={{ padding: 24, minHeight: 280 }}>
    <Route exact path="/" render={() => <Home images={images} tags={tags} />} />
    <Route exact path="/register" render={() => <Register db={db} />} />
    <Route
      exact
      path="/tagList"
      render={() => <TagList images={images} tags={tags} />}
    />
    <Route
      path="/image/:imageId"
      render={() => <ImageEdit images={images} tags={tags} db={db} />}
    />
  </div>
);

export default AppContent;
