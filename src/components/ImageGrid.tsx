import React from 'react';
import { Col, Row } from 'antd';

export const ImageRow: React.FC = ({ children }) => (
  <Row gutter={{ xs: 8, xl: 16 }}>{children}</Row>
);

export const ImageCol: React.FC = ({ children }) => (
  <Col xs={12} sm={8} lg={6} xl={4}>
    {children}
  </Col>
);
