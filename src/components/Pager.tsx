export type Range = {
  start: number;
  end: number;
};

export class Pager {
  private maxItemPerPage: number;

  public constructor(maxItemPerPage: number = 10) {
    this.maxItemPerPage = maxItemPerPage;
  }

  public changePage = (nextPageNumber: number): Range => {
    const start = (nextPageNumber - 1) * this.maxItemPerPage;
    const end = start + this.maxItemPerPage;
    return { start, end };
  };
}
