import React from 'react';
import { Layout } from 'antd';
import { BrowserRouter } from 'react-router-dom';
import { Images, Tags } from './Model';
import AppContent from './AppContent';
import RealtimeDatabaseWrapper from '../fb/RealtimeDatabaseWrapper';
import AppHeader from './AppHeader';

const { Header, Content, Footer } = Layout;

const APP_NAME = 'AorifieR';

type AppProps = {
  db: RealtimeDatabaseWrapper;
};

type AppState = {
  tags: Tags;
  images: Images;
};

class App extends React.Component<AppProps, AppState> {
  public constructor(props: AppProps) {
    super(props);
    this.state = {
      tags: {},
      images: {}
    };
  }

  public componentDidMount() {
    this.db.observe(
      'value',
      (snapshot: firebase.database.DataSnapshot | null) => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const root = snapshot!.val();
        const { tags, images } = root;
        this.setState({
          tags,
          images
        });
      }
    );
  }

  private get db(): RealtimeDatabaseWrapper {
    return this.props.db;
  }

  public render() {
    return (
      <BrowserRouter>
        <Layout className="layout">
          <Header>
            <AppHeader appName={APP_NAME} />
          </Header>
          <Content style={{ background: '#fff' }}>
            <AppContent {...this.state} db={this.db} />
          </Content>
          <Footer style={{ textAlign: 'center' }}>{`${APP_NAME} ©2019`}</Footer>
        </Layout>
      </BrowserRouter>
    );
  }
}
export default App;
