import React from 'react';
import { Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';

type HeaderProps = {
  appName: string;
};

const AppHeader: React.FC<HeaderProps> = ({ appName }) => (
  <React.Fragment>
    <a href="/">
      <div className="app-name">
        {appName}
        <Icon type="fire" />
      </div>
    </a>
    <Menu
      theme="dark"
      mode="horizontal"
      defaultSelectedKeys={[window.location.pathname]}
      style={{ lineHeight: '64px' }}
    >
      <Menu.Item key="/">
        <Link to="/">Home</Link>
      </Menu.Item>
      <Menu.Item key="/register">
        <Link to="/register">New</Link>
      </Menu.Item>
      <Menu.Item key="/tagList">
        <Link to="/tagList">Tags</Link>
      </Menu.Item>
    </Menu>
  </React.Fragment>
);
export default AppHeader;
