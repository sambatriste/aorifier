export interface Image {
  url: string;
  tags: {
    [key: string]: true;
  };
}
export interface Images {
  [key: string]: Image;
}
export interface Tag {
  value: string;
  images: {
    [key: string]: true;
  };
}
export interface Tags {
  [key: string]: Tag;
}
