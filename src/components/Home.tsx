import React from 'react';
import { Pagination } from 'antd';
import { ImageRow, ImageCol } from './ImageGrid';
import ImageCard from './ImageCard';
import { Tags, Images } from './Model';
import { Pager, Range } from './Pager';

type HomeProps = {
  tags: Tags;
  images: Images;
};

type HomeState = Range;

class Home extends React.Component<HomeProps, HomeState> {
  private static MAX_ITEM_NUMBER_PER_PAGE = 7;

  private pager: Pager = new Pager(Home.MAX_ITEM_NUMBER_PER_PAGE);

  public constructor(props: HomeProps) {
    super(props);
    const range = this.pager.changePage(1);
    this.state = range;
  }

  private get numberOfImage() {
    return this.imageKeys.length;
  }

  private get imageKeysOfPage() {
    const { start, end } = this.state;
    return this.imageKeys.slice(start, end);
  }

  private get imageKeys() {
    return Object.keys(this.props.images);
  }

  private handleChange = (page: number) => {
    const renge = this.pager.changePage(page);
    this.setState(renge);
  };

  public render() {
    const { images, tags } = this.props;
    return (
      <div>
        <Pagination
          defaultCurrent={1}
          defaultPageSize={Home.MAX_ITEM_NUMBER_PER_PAGE}
          onChange={this.handleChange}
          total={this.numberOfImage}
        />
        <ImageRow>
          {this.imageKeysOfPage.map((imageId: string) => {
            const image = images[imageId];
            return (
              <ImageCol key={imageId}>
                <ImageCard imageId={imageId} image={image} tags={tags} />
              </ImageCol>
            );
          })}
        </ImageRow>
      </div>
    );
  }
}

export default Home;
