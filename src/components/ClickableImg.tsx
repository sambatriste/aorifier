/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { message } from 'antd';

interface ClickableImgProps {
  url: string;
}

const copyToClipboard = (str: string): void => {
  const temp = document.createElement('div');
  temp.appendChild(document.createElement('pre')).textContent = str;

  const s = temp.style;
  s.position = 'fixed';
  s.left = '-100%';

  document.body.appendChild(temp);
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  document.getSelection()!.selectAllChildren(temp);
  document.execCommand('copy');
  document.body.removeChild(temp);
};

const copyUrl = (evt: React.MouseEvent<HTMLImageElement, MouseEvent>): void => {
  const url = (evt.target as HTMLImageElement).src;
  copyToClipboard(url);
  message.info(`copied. ${url}`);
};

const ClickableImg: React.FC<ClickableImgProps> = ({ url }) => (
  <img src={url} alt={url} onClick={copyUrl} style={{ cursor: 'pointer' }} />
);
export default ClickableImg;
