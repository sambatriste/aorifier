import React from 'react';
import { Form, Icon, Input, Button, message } from 'antd';
import { FormComponentProps } from 'antd/lib/form/Form';

import { Image } from './Model';
import RealtimeDatabaseWrapper from '../fb/RealtimeDatabaseWrapper';

interface RegisterProps extends FormComponentProps {
  db: RealtimeDatabaseWrapper;
}

type RegisterState = {
  url: string;
  imageFound: boolean;
};

class RegisterComponent extends React.Component<RegisterProps, RegisterState> {
  public constructor(props: RegisterProps) {
    super(props);
    this.state = {
      url: '',
      imageFound: false
    };
  }

  public componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  private handleChange = () => {
    if (this.isValidUrl()) {
      const url = this.getUrlFromForm();
      this.setState({ url });
    }
  };

  private handleSubmit = (evt: React.FormEvent) => {
    evt.preventDefault();
    if (!this.isValidUrl()) {
      message.warn('invalid url.');
      return;
    }
    const newImage: Image = {
      url: this.getUrlFromForm(),
      tags: {}
    };
    this.props.db.register('images/', newImage);
    this.setState({
      url: '',
      imageFound: false
    });
    message.info('registered.');
  };

  private getUrlFromForm = () => {
    return this.props.form.getFieldValue('url');
  };

  private isValidUrl = () => {
    const error = this.props.form.getFieldError('url');
    return error === undefined;
  };

  private getImageItemMessage = () => {
    if (this.state.url === '') {
      return '';
    }
    if (this.state.imageFound) {
      return 'Image found.';
    }
    return 'Image not found.';
  };

  public render() {
    return (
      <Form
        layout="vertical"
        onChange={this.handleChange}
        onSubmit={this.handleSubmit}
      >
        <Form.Item label="Image URL">
          {this.props.form.getFieldDecorator('url', {
            rules: [
              { required: true, message: 'Please input image url.' },
              { type: 'url', message: 'Not a valid url.' }
            ],
            initialValue: this.state.url
          })(
            <Input
              prefix={<Icon type="edit" />}
              allowClear
              placeholder="https://example.com/foo.jpg"
            />
          )}
        </Form.Item>

        <Form.Item
          label="preview"
          validateStatus={this.state.imageFound ? 'success' : 'error'}
          help={this.getImageItemMessage()}
        >
          <img
            src={this.state.url}
            alt={this.state.url}
            onError={() => this.setState({ imageFound: false })}
            onLoad={() => this.setState({ imageFound: true })}
          />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            disabled={!this.state.imageFound}
          >
            Register
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const Register = Form.create<RegisterProps>({ name: 'register' })(
  RegisterComponent
);
export default Register;
