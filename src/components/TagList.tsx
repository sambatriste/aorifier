import React from 'react';
import { Tag as AntTag, Divider } from 'antd';
import ImageCard from './ImageCard';
import { ImageRow, ImageCol } from './ImageGrid';
import { Tags, Images } from './Model';

export interface TagListProps {
  tags: Tags;
  images: Images;
}

interface TagListState {
  selectedTagKey: string;
}

export default class TagList extends React.Component<
  TagListProps,
  TagListState
> {
  public constructor(props: TagListProps) {
    super(props);
    this.state = {
      selectedTagKey: ''
    };
  }

  private handleClick = (key: string) => {
    this.setState({
      selectedTagKey: key
    });
  };

  private renderImageListBySelectedTag = () => {
    const tagKey = this.state.selectedTagKey;
    if (this.state.selectedTagKey === '') {
      return <div />;
    }
    return (
      <div>
        <Divider />
        <ImagesByTag
          tagKey={tagKey}
          tags={this.props.tags}
          images={this.props.images}
        />
      </div>
    );
  };

  public render() {
    const { tags } = this.props;
    const { selectedTagKey } = this.state;
    return (
      <div>
        {Object.keys(tags).map(key => {
          const color = key === selectedTagKey ? { color: '#108ee9' } : {};
          return (
            <AntTag key={key} {...color} onClick={() => this.handleClick(key)}>
              {tags[key].value}
            </AntTag>
          );
        })}
        {this.renderImageListBySelectedTag()}
      </div>
    );
  }
}
export interface ImagesByTagProps extends TagListProps {
  tagKey: string;
}

const ImagesByTag: React.FC<ImagesByTagProps> = ({ tags, tagKey, images }) => {
  const selectedTag = tags[tagKey];
  return (
    <ImageRow>
      {Object.keys(selectedTag.images).map(imageId => {
        const image = images[imageId];
        return (
          <ImageCol key={imageId}>
            <ImageCard imageId={imageId} image={image} tags={tags} />
          </ImageCol>
        );
      })}
    </ImageRow>
  );
};
