import React from 'react';
import { Card, Tag, Button } from 'antd';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Image, Tags } from './Model';
import Img from './ClickableImg';

type ImageCardProps = RouteComponentProps<{}> & {
  imageId: string;
  image: Image;
  tags: Tags;
};

const ImageCardComponent: React.FC<ImageCardProps> = ({
  imageId,
  image,
  tags,
  history
}) => {
  const tagsOfImage = image.tags || {};
  return (
    <Card cover={<Img url={image.url} />}>
      {Object.keys(tagsOfImage).map((key: string) => {
        const tag = tags[key];
        return (
          <Tag color="#108ee9" key={`${imageId}-${key}`}>
            {tag.value}
          </Tag>
        );
      })}
      <Button
        shape="circle"
        icon="edit"
        onClick={() => {
          history.push(`/image/${imageId}`);
        }}
      />
    </Card>
  );
};

const ImageCard = withRouter(ImageCardComponent);
export default ImageCard;
