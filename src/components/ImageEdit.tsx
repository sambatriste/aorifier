import React from 'react';
import {
  PageHeader,
  Card,
  Button,
  Spin,
  Form,
  AutoComplete,
  Popconfirm
} from 'antd';
import { SelectValue } from 'antd/lib/select';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Image, Images, Tags, Tag } from './Model';
import Img from './ClickableImg';
import DeletableTag from './DeletableTag';
import RealtimeDatabaseWrapper from '../fb/RealtimeDatabaseWrapper';

type ImageEditProps = RouteComponentProps<{ imageId: string }> & {
  images: Images;
  tags: Tags;
  db: RealtimeDatabaseWrapper;
};

interface ImageEditState {
  tagCandidates: string[];
  newTag: string;
}

class ImageEditComponent extends React.Component<
  ImageEditProps,
  ImageEditState
> {
  public constructor(props: ImageEditProps) {
    super(props);
    this.state = {
      tagCandidates: this.tagCandidates,
      newTag: ''
    };
  }

  private get selectedImage(): Image | undefined {
    const { images, match } = this.props;
    return images[match.params.imageId];
  }

  private get tagCandidates() {
    const image = this.selectedImage;
    return this.doGetTagCandidates(image, this.props.tags);
  }

  private get selectedTags() {
    const image = this.selectedImage;
    if (image === undefined) throw Error('image must not be undefined.');
    return image.tags || {};
  }

  private doGetTagCandidates = (image: Image | undefined, tags: Tags) => {
    // 選択された画像に付けられたタグ
    const selectedTags = image === undefined ? {} : image.tags || {};
    // タグの文言
    const tagLiterals = Object.keys(selectedTags).map(key => {
      return tags[key].value;
    });
    // 候補として表示するタグ
    const tagCandidates = Object.values(tags)
      .map(tag => tag.value)
      .filter(tag => !tagLiterals.includes(tag));
    return tagCandidates;
  };

  private handleChange = (newTag: SelectValue) => {
    this.setState({
      newTag: newTag.toString()
    });
  };

  private handleSubmit = async (evt: React.FormEvent) => {
    evt.preventDefault();

    const updates: { [key: string]: object } = {};
    const { imageId } = this.props.match.params;
    const selectedTags = { ...this.selectedTags };
    const tagKey = this.findTagKey(this.state.newTag);

    // /imagesの更新
    selectedTags[tagKey] = true;
    updates[`images/${imageId}/tags`] = selectedTags;

    // /tagsの更新
    const tag = this.findTag(tagKey);
    tag.images[imageId] = true;
    updates[`tags/${tagKey}`] = tag;
    await this.props.db.update(updates);

    // 追加したタグを入力候補から外す
    const tagCandidates = this.tagCandidates.filter(e => e !== tag.value);
    this.setState({
      tagCandidates,
      newTag: ''
    });
  };

  private findTag = (tagKey: string): Tag => {
    const tag = this.props.tags[tagKey];
    if (tag === undefined) {
      // タグ新規作成時はprops.tagsに要素がない
      return {
        value: this.state.newTag,
        images: {}
      };
    }
    if (tag.images === undefined) {
      // どの画像にも関連付けられていないタグはimagesプロパティが無いので追加する
      tag.images = {};
    }
    return tag;
  };

  private findTagKey = (newTagLiteral: string): string => {
    const { tags } = this.props;
    const tagKey = Object.keys(tags).find(
      key => tags[key].value === newTagLiteral
    );
    if (tagKey !== undefined) {
      return tagKey;
    }
    const newTagKey = this.props.db.newEntry('tags');
    return newTagKey;
  };

  private handleTagClose = async (tagKey: string) => {
    const updates: { [key: string]: object } = {};
    const { imageId } = this.props.match.params;
    const selectedTags = { ...this.selectedTags };
    delete selectedTags[tagKey];
    // /imagesの更新
    updates[`images/${imageId}/tags`] = selectedTags;

    // /tagsの更新
    const tag = { ...this.findTag(tagKey) };
    delete tag.images[imageId];
    updates[`tags/${tagKey}`] = tag;

    await this.props.db.update(updates);

    // 削除したタグを入力候補に戻す
    this.setState(({ tagCandidates }) => {
      tagCandidates.push(tag.value);
      return { tagCandidates };
    });
  };

  private handleDeleteImage = async () => {
    const updates: { [key: string]: null } = {};
    const { imageId } = this.props.match.params;

    // /imagesから削除
    updates[`images/${imageId}`] = null;

    // /tagsから削除
    Object.keys(this.selectedTags).forEach(tagKey => {
      updates[`tags/${tagKey}/images/${imageId}`] = null;
    });
    await this.props.db.update(updates);
    this.props.history.push('/');
  };

  public render() {
    const image = this.selectedImage;
    if (image === undefined) {
      return <Spin />;
    }

    const { tagCandidates, newTag } = this.state;
    const tagsOfImage = image.tags || {};
    return (
      <div>
        <PageHeader title="Edit Image" />
        <Card style={{ width: 400 }} cover={<Img url={image.url} />}>
          <Card.Meta description={image.url} />
        </Card>

        <Form layout="inline" onSubmit={this.handleSubmit}>
          <Form.Item label="Tag">
            <AutoComplete
              dataSource={tagCandidates}
              onChange={this.handleChange}
              value={newTag}
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Add
            </Button>
          </Form.Item>
        </Form>
        {Object.keys(tagsOfImage).map((key: string) => {
          const tag = this.props.tags[key];
          return (
            <DeletableTag
              title="Really remove this tag?"
              onConfirm={() => this.handleTagClose(key)}
              key={key}
            >
              {tag.value}
            </DeletableTag>
          );
        })}
        <Form>
          <Form.Item>
            <Popconfirm
              title="Are you sure delete this task?"
              onConfirm={this.handleDeleteImage}
            >
              <Button title="Delete">Delete</Button>
            </Popconfirm>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const ImageEdit = withRouter(ImageEditComponent);

export default ImageEdit;
