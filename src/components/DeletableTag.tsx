import React from 'react';
import { Tag as AntTag, Popconfirm } from 'antd';
import { PopconfirmProps } from 'antd/lib/popconfirm';

const nop = (e: React.MouseEvent) => {
  e.preventDefault();
};

const DeletableTag: React.FC<PopconfirmProps> = (props: PopconfirmProps) => {
  const { children } = props;
  return (
    <Popconfirm {...props}>
      <AntTag closable color="#f50" onClose={nop}>
        {children}
      </AntTag>
    </Popconfirm>
  );
};
export default DeletableTag;
