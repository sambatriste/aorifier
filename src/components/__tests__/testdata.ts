import { Images, Tags } from '../Model';

test.skip('skip', () => {});

export const images: Images = {
  '1': {
    tags: {
      '1': true
    },
    url:
      'https://stat.ameba.jp/user_images/20141221/18/3052h/84/39/j/o0320028113165121011.jpg'
  },
  '2': {
    tags: {
      '3': true
    },
    url:
      'https://i.pinimg.com/originals/2b/52/68/2b526833510c052bd51819183d2783ca.jpg'
  },
  '3': {
    tags: {
      '4': true
    },
    url: 'http://donsok.com/wp-content/uploads/imgs/4/7/474e46be.jpg'
  }
};
export const tags: Tags = {
  '1': {
    images: {
      '1': true
    },
    value: '笑い'
  },
  '2': {
    images: {
      '1': true
    },
    value: '見下す'
  },
  '3': {
    images: {
      '2': true
    },
    value: '言い訳'
  },
  '4': {
    images: {
      '3': true
    },
    value: 'バカ'
  }
};
