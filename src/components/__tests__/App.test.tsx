import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';
import MockDB from '../mock/MockDB';
import { images, tags } from './testdata';
import Home from '../Home';
import ImageEdit from '../ImageEdit';

const db = new MockDB();

describe('snapshot tests', () => {
  it('Home Component', () => {
    const result = renderer.create(
      <BrowserRouter>
        <Home images={images} tags={tags} />
      </BrowserRouter>
    );
    expect(result).toMatchSnapshot();
  });

  it('ImageEdit Component', () => {
    const result = renderer.create(
      <BrowserRouter>
        <ImageEdit images={images} tags={tags} db={db} />
      </BrowserRouter>
    );
    expect(result).toMatchSnapshot();
  });
});
