import { Pager } from '../Pager';

describe('pager', () => {
  const sut = new Pager();
  it('', () => {
    let range = sut.changePage(1);
    expect(range.start).toBe(0);
    expect(range.end).toBe(10);

    range = sut.changePage(2);
    expect(range.start).toBe(10);
    expect(range.end).toBe(20);
  });
});
