import firebase from 'firebase/app';

const firebaseConfig = {
  apiKey: 'AIzaSyD2r2CBr0Wp5Cy0oGKkHmvW4s5fqaPR5SA',
  authDomain: 'aorifier.firebaseapp.com',
  databaseURL: 'https://aorifier.firebaseio.com',
  projectId: 'aorifier',
  storageBucket: 'aorifier.appspot.com',
  messagingSenderId: '69990612292',
  appId: '1:69990612292:web:bc4ca6d50cf0987f'
};

export const initialize = (config = firebaseConfig): void => {
  firebase.initializeApp(config);
};
