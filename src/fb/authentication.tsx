import firebase from 'firebase/app';
import 'firebase/auth';

export const signInAnonymously = async () => {
  return firebase.auth().signInAnonymously();
};
