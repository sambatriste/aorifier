import Firebase from 'firebase/app';
import 'firebase/database';

export default class RealtimeDataseWrapper {
  public update(updates: { [key: string]: object | null }) {
    return this.ref().update(updates);
  }

  public newEntry(path: string): string {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return this.push(path).key!;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public register(path: string, value: any) {
    return this.push(path).set(value);
  }

  public observe(
    eventType: Firebase.database.EventType,
    callback: (
      a: Firebase.database.DataSnapshot | null,
      b?: string | undefined
    ) => void
  ) {
    this.ref().on(eventType, callback);
  }

  // eslint-disable-next-line class-methods-use-this
  private ref(path?: string | undefined) {
    return Firebase.database().ref(path);
  }

  private push(path: string) {
    return this.ref(path).push();
  }
}
