import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import * as config from './fb/configuration';
import * as auth from './fb/authentication';
import RealtimeDatabaseWrapper from './fb/RealtimeDatabaseWrapper';

config.initialize();

auth.signInAnonymously().then(() => {
  const db = new RealtimeDatabaseWrapper();
  ReactDOM.render(<App db={db} />, document.getElementById('root'));
});
